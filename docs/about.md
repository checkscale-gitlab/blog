---
title: About me
subtitle: I'm Cody and I love to code!
comments: false
---


### A Little about me

I'm a developer with a background in distributed systems, data analytics, ML and big data.
When I'm not coding, I love to cook and you can find me either riding my bike or at jiu-jitsu class.
